package kz.aitu.chat.service.interfaces;

import kz.aitu.chat.model.Message;

import java.util.List;

public interface IMessageService extends IService<Message> {
    List<Message> findMessagesByChatId(Long chatId);

    List<Message> getlasttenbychatid(Long id);

    List<Message> getlasttenbyuserid(Long id);

}
