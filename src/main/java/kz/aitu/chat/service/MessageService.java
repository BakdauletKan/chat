package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.service.interfaces.IMessageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessageService implements IMessageService {
    private final MessageRepository messageRepository;

    @Override
    public void add(Message o) {
        messageRepository.save(o);
    }

    @Override
    public List<Message> getAll() {
        return messageRepository.findAll();
    }

    @Override
    public void update(Message message) {
        messageRepository.save(message);
    }

    @Override
    public void delete(Message message) {
        messageRepository.delete(message);
    }

    @Override
    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

    @Override
    public Message getItemById(Long id) {
        return messageRepository.findById(id).get();
    }

    @Override
    public List<Message> findMessagesByChatId(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    @Override
    public List<Message> getlasttenbychatid(Long id) {
        return messageRepository.getLast10ByChatId(id);
    }

    @Override
    public List<Message> getlasttenbyuserid(Long id) {
        return messageRepository.getLast10ByUserId(id);
    }


}
