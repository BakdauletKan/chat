package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.AuthRepository;
import kz.aitu.chat.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class AuthService {

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private UserRepository userRepository;

    public Auth login(String login, String password){
        ArrayList<Auth> auths = (ArrayList<Auth>) authRepository.findAll();
        for(Auth auth : auths){
            if(auth.getPassword().equals(password) && auth.getLogin().equals(login)){
                auth.setToken();
                auth.setLastLoginTimestamp();
                return auth;
            }
        }
        return null;
    }

    public Auth register(String login, String password){
        Auth auth = new Auth(login,password);
        User newUser = new User();
        auth.setUserId(newUser.getId());
        userRepository.save(newUser);
        return authRepository.save(auth);
    }

    public Auth getUserWithToken(UUID token){
        ArrayList<Auth> users = (ArrayList<Auth>) authRepository.findAll();
        for(Auth auth : users){
            if(auth.getToken() == token){
                return auth;
            }
        }
        return null;
    }
}
