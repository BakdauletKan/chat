package kz.aitu.chat.service;

import kz.aitu.chat.model.User;
import kz.aitu.chat.repository.UserRepository;
import kz.aitu.chat.service.interfaces.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserService implements IUserService {
    private final UserRepository userRepository;

    @Override
    public void add(User o) {
        userRepository.save(o);
    }

    @Override
    public List<User> getAll() {

        return userRepository.findAll();
    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getItemById(Long id) {
        return userRepository.findById(id).get();
    }
}
