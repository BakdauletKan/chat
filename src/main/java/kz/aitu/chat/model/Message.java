package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "message")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "chat_id")
    private Long chatId;

    private String text;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_read")
    private boolean isRead;

    @Column(name = "is_delivered")
    private boolean isDelivered;

    @Column(name = "read_time")
    private Long readTimeStamp;

    @Column(name = "deliver_time")
    private Long deliveredTimeStamp;

    @Column(name = "message_type")
    private String messageType;

}
