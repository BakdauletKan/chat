package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "Auth")
@NoArgsConstructor
@AllArgsConstructor
public class Auth {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    @Column(name = "lastLoginTimestamp")
    private Long lastLoginTimestamp;
    @Column(name = "token")
    private UUID token;

    public Auth(String login, String password) {
        this.login = login;
        this.password = password;
        this.lastLoginTimestamp = new Date().getTime();
        setToken();
    }

    public Auth(Long userId, String login, String password) {
        this.userId = userId;
        this.login = login;
        this.password = password;
        setToken();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setLastLoginTimestamp(Long lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp() {
        this.lastLoginTimestamp = new Date().getTime();
    }

    public UUID getToken() {
        return token;
    }

    public void setToken() {
        this.token = UUID.randomUUID();
    }
}
