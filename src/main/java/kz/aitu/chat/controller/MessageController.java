package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@AllArgsConstructor
@RequestMapping("/api/message")
public class MessageController {
    private final MessageService messageService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(messageService.getAll());
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Message message) {
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime()/1000);
        messageService.add(message);
        message.setDelivered(true);
        message.setDeliveredTimeStamp(date.getTime()/1000);
        return ResponseEntity.ok("Message was added!");
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody Message message) {
        Date date = new Date();
        message.setUpdatedTimestamp(date.getTime()/1000);
        messageService.update(message);
        return ResponseEntity.ok(message);
    }

    @DeleteMapping("")
    public ResponseEntity<?> delete(@RequestBody Message message) {
        messageService.delete(message);
        return ResponseEntity.ok("Message was deleted!");
    }

    @GetMapping("/get-message/{id}")
    public ResponseEntity<?> getMessage(@PathVariable Long id){
        Message message = messageService.getItemById(id);
        message.setRead(true);
        message.setReadTimeStamp(new Date().getTime()/1000);
        return ResponseEntity.ok(message);
    }

    @GetMapping("/get-messages/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId) {
        return ResponseEntity.ok(messageService.findMessagesByChatId(chatId));
    }

    @GetMapping("/getLast10ByChatId/{id}")
    public ResponseEntity<?> getlasttenbychatid(@PathVariable("id") Long id){
        return ResponseEntity.ok(messageService.getlasttenbychatid(id));
    }

    @GetMapping("/getLast10ByUserId/{id}")
    public ResponseEntity<?> getlasttenbyuserid(@PathVariable("id") Long id){
        return ResponseEntity.ok(messageService.getlasttenbyuserid(id));
    }

//    @GetMapping("/getNotDeliveredMessages/")
//    public ResponseEntity<?> getNotDeliveredMessages(){
//        return ResponseEntity.noContent().build();
//    }
}
