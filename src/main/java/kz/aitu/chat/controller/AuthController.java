package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
@Component
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("signIn/{password}/{login}")
    public ResponseEntity<?> signIn(@PathVariable String login, @PathVariable String password) {
        Auth auth = authService.login(login, password);
        ResponseEntity<?> authResponseEntity = ResponseEntity.ok(auth);
        authResponseEntity.getHeaders().set("TOKEN-KEY",auth.getToken().toString());
        return authResponseEntity;
    }

    @PostMapping("signUp/{password}/{login}")
    public ResponseEntity<?> signUp(@PathVariable String login, @PathVariable String password){
        Auth auth = authService.register(login, password);
        ResponseEntity<?> authResponseEntity = ResponseEntity.ok(auth);
        authResponseEntity.getHeaders().set("TOKEN-KEY",auth.getToken().toString());
        return authResponseEntity;
    }
}
