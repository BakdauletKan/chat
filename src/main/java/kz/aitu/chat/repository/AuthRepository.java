package kz.aitu.chat.repository;
import kz.aitu.chat.model.Auth;
import org.springframework.data.repository.CrudRepository;

public interface AuthRepository extends CrudRepository<Auth, Integer> {
}
